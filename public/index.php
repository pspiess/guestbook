<?php

use System\App;

require_once dirname(__FILE__) . '/System/Autoloader.php';

$config = dirname(__FILE__) . '/System/config.php';

//create new app use config and run
App::create($config)->run();
