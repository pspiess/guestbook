DROP TABLE IF EXISTS `entries`;

CREATE TABLE `entries` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user` varchar(50) NOT NULL,
  `text` text,
  `date` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user` (`user`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

INSERT INTO `entries` VALUES
(1,'Ryu', 'Hadouken',1577923200),
(2,'Ken', 'Shoryuken',1588809600),
(3,'Guile', 'Sonic Boom',1546473600),
(4,'Mario', 'Its Mario', 957830400),
(5,'Sonic', 'Wruuuuuuuuuuuu', 1542672000)
