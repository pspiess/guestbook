<?php

namespace App\Controllers;

use App\Models\Entries;
use System\Controller;

class IndexController extends Controller
{
    /**
     * show the form with all guestbook entries
     */
    public function actionIndex()
    {
        $entries = Entries::getAll();
        //ToDo: Token for CSRF protection
        //$token = bin2hex(random_bytes(32));
        //$_SESSION['token'] = $token;
        echo $this->render('index', true, array('entries' => $entries));
    }

    /**
     * add one entry to guestbook
     */
    public function actionAdd()
    {
        if (isset($_POST['submit'])) {
            //ToDo: csrf protection
//            if ($_SESSION['token'] !== $_POST['token']) {}
            try {
                //ToDo: Email validation/verification
                $username = substr($_POST['username'], 0, 50);
                $text = $_POST['text'];

                $res = Entries::add($username, $text);
                if (!$res) {
                    throw new \Exception('Upps... Something went wrong, please try again later');
                }

                echo $this->render('thank-you', true, array('username' => $username));
            } catch (\Exception $e) {
                echo $this->render('index', true, array('error' => $e->getMessage(), 'entries' => Entries::getAll()));
            }
        }
    }
}

