<div class="row">
    <?php if (!empty($error)): ?>
        <div class="alert alert-danger" role="alert">
            <?php echo $error; ?>
        </div>
    <?php endif; ?>
</div>
<div class="row">
    <div id="form-group">
        <form id="comment_form" method="post" action="/index/add">
            <!--            <input type="hidden" name="token" value="--><?php //echo $token; ?><!--">-->
            <div class="form-group">
                <label for="username">Your name</label>
                <input type="text" class="form-control" id="username" name="username" maxlength="50" required aria-describedby="usernameHelp">
                <small id="usernameHelp" class="form-text text-muted">Please type your username, should be maximum 50 characters</small>
            </div>
            <div class="form-group">
                <label for="username">Your text</label>
                <textarea class="form-control" id="username" name="text" aria-describedby="textHelp"></textarea>
                <small id="textHelp" class="form-text text-muted">Please type your feelings</small>
            </div>
            <input class="btn btn-primary" name="submit" type="submit" value="Leave guestbook entry">
        </form>
    </div>
</div>

<div class="row" style="padding-top: 2.0em">
    </hr>
    <h2>All guests</h2>
</div>
<div class="row">
    <table class="table">
        <thead>
        <tr>
            <th scope="col">Date</a></th>
            <th scope="col">Username</a></th>
            <th scope="col">Entry</th>
        </thead>
        <tbody>
        <?php
        if (!empty($entries)) :
            foreach ($entries as $entry) :
                $date = new \DateTime('@' . $entry['date']);
                ?>
                <tr>
                    <td><?php echo $date->format('d-m-Y'); ?></td>
                    <td><?php echo $entry['user'] ?></td>
                    <td><?php echo htmlspecialchars($entry['text']) ?></td>
                </tr>
            <?php
            endforeach;
        endif;
        ?>
        </tbody>
    </table>
</div>
