<?php

namespace App\Models;

use System\App;

class Entries
{
    // get all guestbook entries
    public static function getAll()
    {
        $sql = "SELECT * FROM entries ORDER BY date DESC";

        $query = App::get()->db->prepare($sql);
        $query->execute();
        return $query->fetchAll(\PDO::FETCH_ASSOC);
    }

    // add new guestbook entry to db
    public static function add(string $name, string $text)
    {
        $sql = 'INSERT INTO entries (user, text, date) VALUES (?, ?, ?)';
        $query = App::get()->db->prepare($sql);
        $query->bindParam(1, $name, \PDO::PARAM_STR);
        $query->bindParam(2, $text, \PDO::PARAM_LOB);
        $query->bindParam(3, time(), \PDO::PARAM_INT);
        return $query->execute();
    }
}

