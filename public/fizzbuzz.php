<?php

print "$argc arguments were passed. In order: \n";

for ($i = 1; $i <= $argc - 1; ++$i) {
    print_r(printOutput((int)$argv[$i]));
    //var_dump(printOutput((int)$argv[$i]));
}

function printOutput(int $inputNumber): array
{
    $output = [];

    for ($i = 1; $i <= $inputNumber; ++$i) {
        if ($i % 3 === 0 && $i % 5 === 0) {
            $output[$i] = 'FIZZBUZZ';
            //array_push($output, 'FIZZBUZZ');
        } elseif ($i % 3 === 0) {
            $output[$i] = 'FIZZ';
            //array_push($output, 'FIZZ');
        } elseif ($i % 5 === 0) {
            $output[$i] = 'BUZZ';
            //array_push($output, 'BUZZ');
        } else {
            $output[$i] = $i;
            //array_push($output, $i);
        }
    }

    return $output;
}
