<?php

namespace System;

use Exception;
use PDO;
use PDOException;

class Application
{
    public $db;
    public $controller;
    public $config;
    public $basePath;

    // create application instance
    // create db instance
    public function __construct(array $config)
    {
        $this->config = $config;
        $this->basePath = dirname(__DIR__);

        if (!empty($config['db'])) {
            try {
                $this->db = new PDO($config['db']['connectionString'], $config['db']['user'], $config['db']['pass'], array(PDO::ATTR_PERSISTENT => true));
            } catch (PDOException $e) {
                echo "Error PDO: " . $e->getMessage();
                die();
            }
        }
    }

    // find the route by controller and action, check the request string
    public function run(): bool
    {
        $route = $this->getRoute();

        list($controllerName, $actionName) = explode('/', "$route/");

        if (empty($actionName)) $actionName = 'index';

        $this->setController($controllerName);
        $this->setAction($actionName, $route);

        return true;
    }

    private function getRoute()
    {
        $uri = $_SERVER['REQUEST_URI'];
        if (($pos = strpos($uri, '?')) !== false) {
            $uri = substr($uri, 0, $pos);
        }
        $uri = trim($uri, '/ ');

        return !empty($uri) ? $uri : $this->config['defaultRoute'];
    }

    /**
     * set the controller
     */
    private function setController(string $controllerName)
    {
        try {
            $controllerClass = 'App\\Controllers\\' . ucfirst(strtolower($controllerName)) . 'Controller';
            $this->controller = new $controllerClass;
        } catch (Exception $e) {
            echo 'Can\'t find controller: ' . var_export($controllerName, true);
            die();
        }
    }

    /**
     * enrich controller with action
     */
    private function setAction(string $actionName, string $route)
    {
        $actionMethod = 'action' . ucfirst(strtolower($actionName));
        if (method_exists($this->controller, $actionMethod)) {
            $this->controller->$actionMethod();
        } else {
            echo 'Can\'t run action: ' . var_export($route, true);
            die();
        }
    }
}
