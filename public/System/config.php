<?php
return array(
    'db' => array(
        'user' => 'dbuser',
        'pass' => 'password',
        'connectionString' => 'mysql:host=mysql;dbname=dbname',
    ),
    'defaultRoute' => 'index/index',
    'admin' => 'admin-pw'
);
