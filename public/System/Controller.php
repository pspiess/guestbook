<?php

namespace System;

class Controller
{
    const LAYOUT = 'basic';

    public function render(string $viewName, bool $useLayout = true, array $params = array())
    {
        //get classname with namespace
        $className = end(explode('\\', get_class($this)));
        $controllerName = substr($className, 0, strpos($className, 'Controller'));

        $viewPath = App::get()->basePath . '/App/Views/' . $controllerName . '/' . strtolower($viewName) . '.php';
        if (!file_exists($viewPath)) {
            echo 'Can\'t find view file: ' . $viewPath;
            die();
        }

        $view = $this->renderFile($viewPath, $params);

        // view file content is put into layout file content
        if ($useLayout) {
            $layoutPath = App::get()->basePath . '/App/Views/Layout/' . self::LAYOUT . '.php';
            if (!file_exists($layoutPath)) {
                echo 'Can\'t find layout file';
                die();
            }
            $layout = $this->renderFile($layoutPath, array('content' => $view));
        }

        if ($useLayout) {
            $content = $layout;
        } else {
            $content = $view;
        }

        return $content;
    }

    /**
     * renders the files with params
     */
    private function renderFile(string $path, array $params = array())
    {
        ob_start();
        ob_implicit_flush(false);
        extract($params);
        require($path);
        return ob_get_clean();
    }
}
