<?php

namespace System;

class App
{
    protected static $instance;

    // create application instance
    public static function create(string $configPath): Application
    {
        if (empty($configPath)) {
            $configPath = $_SERVER['DOCUMENT_ROOT'] . '/App/System/config.php';
        }

        if (file_exists($configPath)) {
            $config = require_once($configPath);
        } else {
            echo 'Can\'t open config file ' . var_export($configPath, true);
            die();
        }
        $app = new Application($config);
        self::$instance = $app;
        return $app;
    }

    // check if application instance is already present, create new of not
    public static function get(): Application
    {
        if (is_null(self::$instance)) {
            self::create();
        }
        return self::$instance;
    }

}

